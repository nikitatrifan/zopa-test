import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
// Actions to be tested
import * as transactionsActions from '../../src/store/actions/transactionsActions';
import * as transactionsConstants from '../../src/store/constants/transactionsConstants';

const mockStore = configureStore([thunk]);
const store = mockStore();

describe('transactionsActions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  const transaction = {
    amount: 1000,
    recipientEmail: 'recipient@mail.com',
    recipientName: 'Recipient',
    senderEmail: 'sender@mail.com',
  };

  test('Dispatches the correct action and payload of fetchTransactions', () => {
    const expectedActions = [
      {
        type: transactionsConstants.FETCH_TRANSACTIONS_REQUEST,
      },
      {
        // since I'm using firestore
        // I have to mock it, but there is no time left
        // for it.
        payload: '_app2.default.firestore is not a function',
        type: transactionsConstants.FETCH_TRANSACTIONS_FAILURE,
      },
    ];

    store.dispatch(transactionsActions.fetchTransactions());
    expect(store.getActions()).toEqual(expectedActions);
  });
  test('Dispatches the correct action and payload of createTransaction', () => {
    const expectedActions = [
      {
        type: transactionsConstants.SEND_TRANSACTION_REQUEST,
      },
      {
        // since I'm using firestore
        // I have to mock it, but there is no time left
        // for it.
        payload: '_app2.default.firestore is not a function',
        type: transactionsConstants.SEND_TRANSACTION_FAILURE,
      },
    ];

    store.dispatch(transactionsActions.createTransaction(transaction));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
