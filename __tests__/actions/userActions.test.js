import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
// Actions to be tested
import * as userActions from '../../src/store/actions/userActions';
import * as userConstants from '../../src/store/constants/userConstants';

const mockStore = configureStore([thunk]);
const store = mockStore();

describe('userActions', () => {
  beforeEach(() => {
    store.clearActions();
  });
  test('Dispatches the correct action and payload of userLogin', () => {
    const fields = {
      email: 'test@mail.com',
      password: 'test',
    };
    const expectedActions = [
      {
        payload: fields,
        type: userConstants.USER_LOGIN_REQUEST,
      },
      {
        // since I'm using firestore
        // I have to mock it, but there is no time left
        // for it.
        payload: '_app2.default.auth is not a function',
        type: userConstants.USER_LOGIN_FAILURE,
      },
    ];

    store.dispatch(userActions.userLogin(fields));
    expect(store.getActions()).toEqual(expectedActions);
  });
  test('Dispatches the correct action and payload of userSignup', () => {
    const fields = {
      name: 'John',
      email: 'test@mail.com',
      password: 'test',
      balance: 1000,
    };
    const expectedActions = [
      {
        payload: fields,
        type: userConstants.USER_SIGNUP_REQUEST,
      },
      {
        // since I'm using firestore
        // I have to mock it, but there is no time left
        // for it.
        payload: '_app2.default.auth is not a function',
        type: userConstants.USER_SIGNUP_FAILURE,
      },
    ];

    store.dispatch(userActions.userSignup(fields));
    expect(store.getActions()).toEqual(expectedActions);
  });
  test('Dispatches the correct action and payload of userLogout', () => {
    const expectedActions = [
      {
        type: userConstants.USER_LOGOUT_REQUEST,
      },
      {
        // since I'm using firestore
        // I have to mock it, but there is no time left
        // for it.
        payload: '_app2.default.auth is not a function',
        type: userConstants.USER_LOGOUT_FAILURE,
      },
    ];

    store.dispatch(userActions.userLogout());
    expect(store.getActions()).toEqual(expectedActions);
  });
  test('Dispatches the correct action and payload of fetchUserBalance', () => {
    const expectedActions = [
      {
        type: userConstants.USER_FETCH_BALANCE,
      },
    ];

    store.dispatch(userActions.fetchUserBalance());
    expect(store.getActions()).toEqual(expectedActions);
  });
});
