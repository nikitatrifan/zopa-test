import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import AccountStatistics from '../../src/components/AccountStatistics';

describe('<AccountStatistics />', () => {
  describe('render()', () => {
    test('renders the component', () => {
      const wrapper = shallow(<AccountStatistics totalSpent={100} balance={1000} />);
      const component = wrapper.dive();

      expect(toJson(component)).toMatchSnapshot();
    });
  });
});
