import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import noop from '../../src/helpers/noop';

import AuthForm from '../../src/components/AuthForm';
import TextInput from '../../src/components/TextInput';

describe('<AuthForm />', () => {
  describe('render()', () => {
    test('renders the component', () => {
      const wrapper = shallow(<AuthForm isLogin isError={false} errorMessage="" />);
      const component = wrapper.dive();

      expect(toJson(component)).toMatchSnapshot();
    });

    test('renders the component with login form', () => {
      const wrapper = shallow(
        <AuthForm isLogin isError={false} errorMessage="" />,
      );
      const component = wrapper.dive();
      const inputs = component.find(TextInput);

      expect(inputs.length).toEqual(2);
    });

    test('renders the component with sign up form', () => {
      const wrapper = shallow(
        <AuthForm isLogin={false} isError={false} errorMessage="" />,
      );
      const component = wrapper.dive();
      const inputs = component.find(TextInput);

      expect(inputs.length).toEqual(4);
    });
  });

  describe('Events', () => {
    test('should respond to change event and change the state of the AuthForm Component', () => {
      const wrapper = shallow(
        <AuthForm isLogin={false} isError={false} errorMessage="" />,
      );
      const component = wrapper.dive();
      const testEmail = 'test@gmail.com';
      const emailInput = component.findWhere((w: Object): boolean => w.prop('name') === 'email');
      emailInput.simulate('change', { target: { name: 'email', value: testEmail } });

      expect(component.state('email')).toEqual(testEmail);
    });
    test('should submit form without errors', () => {
      const mockSubmit = jest.fn();
      const wrapper = shallow(
        <AuthForm onSubmit={mockSubmit} isLogin={true} isError={false} errorMessage="" />,
      );
      const component = wrapper.dive();
      const formNode = component.find('form');

      formNode.simulate('submit', { preventDefault: noop });

      expect(mockSubmit.mock.calls.length).toEqual(1);
    });
  });
});
