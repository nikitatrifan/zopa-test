import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Box from '../../src/components/Box';

describe('<Box />', () => {
  describe('render()', () => {
    test('renders the component correctly', () => {
      const wrapper = shallow(<Box />);
      const component = wrapper.dive();

      expect(toJson(component)).toMatchSnapshot();
    });
  });
});
