import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Button from '../../src/components/Button';

describe('<Button />', () => {
  describe('render()', () => {
    test('renders the component correctly', () => {
      const wrapper = shallow(
        <Button>
          Click
        </Button>,
        );
      const component = wrapper.dive();

      expect(toJson(component)).toMatchSnapshot();
    });
  });
  describe('onClick()', () => {
    test('successfully calls the onClick handler', () => {
      const mockOnClick = jest.fn();
      const wrapper = shallow(
        <Button onClick={mockOnClick}>
          Click
        </Button>,
      );
      const component = wrapper.dive();

      component.find('button').simulate('click');

      expect(mockOnClick.mock.calls.length).toEqual(1);
    });
  });
});
