import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Container from '../../src/components/Container';

describe('<Container />', () => {
  describe('render()', () => {
    test('renders the component correctly', () => {
      const wrapper = shallow(
        <Container>
          <span>Test</span>
        </Container>,
      );
      const component = wrapper.dive();

      expect(toJson(component)).toMatchSnapshot();
    });
  });
});
