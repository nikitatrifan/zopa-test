import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import DataPlaceholder from '../../src/components/DataPlaceholder';

describe('<DataPlaceholder />', () => {
  describe('render()', () => {
    test('renders the component correctly with default props', () => {
      const wrapper = shallow(
        <DataPlaceholder />,
      );
      const component = wrapper.dive();

      expect(toJson(component)).toMatchSnapshot();
    });
  });
});
