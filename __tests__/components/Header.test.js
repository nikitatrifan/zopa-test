import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Header from '../../src/components/Header';
import Paragraph from '../../src/components/Paragraph';

describe('<Header />', () => {
  describe('render()', () => {
    const testUserName = 'John Doe';
    test('renders the component correctly', () => {
      const wrapper = shallow(
        <Header userName={testUserName} />,
      );
      const component = wrapper.dive();

      expect(toJson(component)).toMatchSnapshot();
    });
    test('renders the component userName prop correctly', () => {
      const wrapper = shallow(
        <Header userName={testUserName} />,
      );
      const component = wrapper.dive();
      const userNameNode = component.find(Paragraph).findWhere((w: Object): boolean => (
        w.text().indexOf(testUserName) !== -1
      ));

      expect(userNameNode.length).toEqual(1);
    });
  });
});
