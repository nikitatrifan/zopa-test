import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Heading from '../../src/components/Heading';

describe('<Heading />', () => {
  describe('render()', () => {
    test('renders the component correctly', () => {
      const wrapper = shallow(
        <Heading>
          Test
        </Heading>,
      );
      const component = wrapper.dive();

      expect(toJson(component)).toMatchSnapshot();
    });
    test('renders color prop the Heading component correctly', () => {
      const testColor = '#121212';
      const wrapper = shallow(
        <Heading color={testColor}>
          Test
        </Heading>,
      );
      const component = wrapper.dive();
      const styleProp = component.prop('style');

      expect(styleProp.color).toEqual(testColor);
    });
    test('renders align prop the Heading component correctly', () => {
      const alignProp = 'center';
      const wrapper = shallow(
        <Heading align={alignProp}>
          Test
        </Heading>,
      );
      const component = wrapper.dive();
      const styleProp = component.prop('style');

      expect(styleProp.textAlign).toEqual(alignProp);
    });
    test('renders size prop of the Heading component correctly', () => {
      const size = 1;
      const wrapper = shallow(
        <Heading size={size}>
          Test
        </Heading>,
      );
      const component = wrapper.dive();
      const tagName = component.name();

      expect(tagName).toEqual(`h${size}`);
    });
  });
});
