import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Link from '../../src/components/Link';

describe('<Link />', () => {
  describe('render()', () => {
    const testPath = '/auth';
    test('renders the component correctly', () => {
      const wrapper = shallow(
        <Link to={testPath}>
          Test
        </Link>,
      );
      const component = wrapper.dive();

      expect(toJson(component)).toMatchSnapshot();
    });
    test('renders "to" prop of the component correctly', () => {
      const wrapper = shallow(
        <Link to={testPath}>
          Test
        </Link>,
      );
      const component = wrapper.dive();
      const hrefProp = component.prop('href');
      expect(hrefProp).toEqual(hrefProp);
    });
  });
});
