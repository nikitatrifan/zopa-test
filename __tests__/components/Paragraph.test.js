import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Paragraph from '../../src/components/Paragraph';

describe('<Paragraph />', () => {
  describe('render()', () => {
    test('renders the component correctly', () => {
      const wrapper = shallow(
        <Paragraph>
          Test
        </Paragraph>,
      );
      const component = wrapper.dive();

      expect(toJson(component)).toMatchSnapshot();
    });
    test('renders color prop the Paragraph component correctly', () => {
      const testColor = '#121212';
      const wrapper = shallow(
        <Paragraph color={testColor}>
          Test
        </Paragraph>,
      );
      const component = wrapper.dive();
      const styleProp = component.prop('style');

      expect(styleProp.color).toEqual(testColor);
    });
    test('renders align prop the Paragraph component correctly', () => {
      const alignProp = 'center';
      const wrapper = shallow(
        <Paragraph align={alignProp}>
          Test
        </Paragraph>,
      );
      const component = wrapper.dive();
      const styleProp = component.prop('style');

      expect(styleProp.textAlign).toEqual(alignProp);
    });
  });
});
