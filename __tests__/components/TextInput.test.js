import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import noop from '../../src/helpers/noop';

import TextInput from '../../src/components/TextInput';

describe('<TextInput />', () => {
  describe('render()', () => {
    test('renders the component correctly', () => {
      const wrapper = shallow(
        <TextInput />,
      );
      const component = wrapper.dive();

      expect(toJson(component)).toMatchSnapshot();
    });
  });
  describe('onChange()', () => {
    test('should respond to change event and change the state of the TextInput Component', () => {
      const wrapper = shallow(
        <TextInput onChange={noop} name="name" />,
      );
      const component = wrapper.dive();
      component.find('input').simulate('change', { target: { name: 'name', value: 'John' } });

      expect(component.state('value')).toEqual('John');
    });
  });
});
