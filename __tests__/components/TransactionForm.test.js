import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import noop from '../../src/helpers/noop';

import TransactionForm from '../../src/components/TransactionForm';

describe('<TransactionForm />', () => {
  describe('render()', () => {
    test('renders the component', () => {
      const wrapper = shallow(<TransactionForm isLogin isError={false} errorMessage="" />);
      const component = wrapper.dive();

      expect(toJson(component)).toMatchSnapshot();
    });
  });

  describe('Events', () => {
    test('should respond to change event and change the state of the TransactionForm Component', () => {
      const wrapper = shallow(
        <TransactionForm isError={false} onSubmit={noop} errorMessage="" />,
      );
      const component = wrapper.dive();
      const testEmail = 'test@gmail.com';
      const emailInput = component.findWhere((w: Object): boolean => w.prop('name') === 'email');
      emailInput.simulate('change', { target: { name: 'email', value: testEmail } });

      expect(component.state('email')).toEqual(testEmail);
    });
    test('should change state on change input event', () => {
      const testName = 'John';
      const wrapper = shallow(
        <TransactionForm disabled={false} isError={false} onSubmit={noop} errorMessage="" />,
      );
      const component = wrapper.dive();
      const formNode = component.find('form');
      const input = component.findWhere(
        (w: Object): boolean => (
          w.prop('name') === 'name'
        ),
      );

      input.simulate('change', { target: { name: 'name', value: testName } });

      formNode.simulate('submit', { preventDefault: noop });

      expect(component.state().name).toEqual(testName);
    });
    test('should submit form without errors', () => {
      const mockSubmit = jest.fn();
      const wrapper = shallow(
        <TransactionForm disabled={false} isError={false} onSubmit={mockSubmit} errorMessage="" />,
      );
      const component = wrapper.dive();
      const findInputByName = (name: string): Function => component.findWhere(
        (w: Object): boolean => (
          w.prop('name') === name
        ),
      );
      const formNode = component.find('form');

      findInputByName('name').simulate('change', { target: { name: 'name', value: 'John' } });
      findInputByName('email').simulate('change', { target: { name: 'email', value: 'test@test.com' } });
      findInputByName('amount').simulate('change', { target: { name: 'amount', value: '1000' } });

      formNode.simulate('submit', { preventDefault: noop });

      expect(component.state().name).toEqual('John');
    });
  });
});
