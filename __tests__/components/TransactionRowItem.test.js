import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import TransactionRowItem from '../../src/components/TransactionRowItem';

describe('<TransactionRowItem />', () => {
  const testData = {
    amount: 10000,
    recipientEmail: 'john@email.com',
    recipientName: 'John',
    senderEmail: 'nancy@email.com',
  };

  describe('render()', () => {
    test('renders the component correctly', () => {
      const wrapper = shallow(
        <TransactionRowItem data={testData} />,
      );

      const component = wrapper.dive();

      expect(toJson(component)).toMatchSnapshot();
    });
  });
});
