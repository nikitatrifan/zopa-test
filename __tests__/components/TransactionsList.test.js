import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import TransactionsList from '../../src/components/TransactionsList';

describe('<TransactionsList />', () => {
  const testData = [{
    amount: 10000,
    recipientEmail: 'john@email.com',
    recipientName: 'John',
    senderEmail: 'nancy@email.com',
  }];

  describe('render()', () => {
    test('renders the component correctly', () => {
      const wrapper = shallow(
        <TransactionsList data={testData} />,
      );

      const component = wrapper.dive();

      expect(toJson(component)).toMatchSnapshot();
    });
  });
});
