import transactionsReducer from '../../src/store/reducers/transactions';
import {
  FETCH_TRANSACTIONS_SUCCESS,
  SEND_TRANSACTION_SUCCESS,
} from '../../src/store/constants/transactionsConstants';

describe('transactions reducer', () => {
  const initialState = {
    isFetching: false,
    isError: false,
    errorMessage: null,
    data: [],
  };
  const transaction = {
    amount: 1000,
    recipientEmail: 'recipient@mail.com',
    recipientName: 'Recipient',
    senderEmail: 'sender@mail.com',
  };
  describe('INITIAL_STATE', () => {
    test('is correct', () => {
      const action = { type: '' };
      expect(transactionsReducer(undefined, action)).toEqual(initialState);
    });
  });
  describe('Transactions', () => {
    test('returns the correct state when transaction has been sent', () => {
      const action = { type: SEND_TRANSACTION_SUCCESS, payload: transaction };
      const resultState = {
        ...initialState,
        isFetching: false,
        data: [transaction],
      };
      expect(transactionsReducer(undefined, action)).toEqual(resultState);
    });
    test('returns the correct state when transactions has been fetched', () => {
      const action = { type: FETCH_TRANSACTIONS_SUCCESS, payload: [transaction] };
      const resultState = {
        ...initialState,
        isFetching: false,
        data: [transaction],
      };
      expect(transactionsReducer(undefined, action)).toEqual(resultState);
    });
  });
});
