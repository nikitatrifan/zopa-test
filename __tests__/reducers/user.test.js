import userReducer from '../../src/store/reducers/user';
import {
  USER_LOGOUT_REQUEST,
} from '../../src/store/constants/userConstants';

describe('user reducer', () => {
  const initialState = {
    isFetching: false,
    isError: false,
    isAuthenticated: false,
    errorMessage: null,
    email: null,
    password: null,
  };
  describe('INITIAL_STATE', () => {
    test('is correct', () => {
      const action = { type: '' };
      expect(userReducer(undefined, action)).toEqual(initialState);
    });
  });
  describe('Auth', () => {
    test('returns the correct state when logout', () => {
      const action = { type: USER_LOGOUT_REQUEST };
      const resultState = {
        ...initialState,
        isFetching: true,
        isAuthenticated: false,
      };
      expect(userReducer(undefined, action)).toEqual(resultState);
    });
  });
});
