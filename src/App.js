import React, { Component } from 'react';
import {
  Switch, Route, Redirect, withRouter,
} from 'react-router-dom';
import { TweenMax } from 'gsap';
import Helmet from 'react-helmet';
import AuthPage from './routes/AuthPage';
import LogoutPage from './routes/LogoutPage';
import MoneyTransferPage from './routes/MoneyTransferPage';

type Props = {
  location: {
    key: string,
  },
};
type State = null;
type SnapshotType = boolean | null;

class App extends Component {
  componentDidUpdate(prevProps: Props, prevState: State, snapshot: SnapshotType) {
    // show a content if there was a snapshot
    if (snapshot) {
      TweenMax.to(this.wrapper, 0.6, {
        opacity: 1,
      });
    }
  }

  getSnapshotBeforeUpdate(prevProps: Props): SnapshotType {
    // flash transition between pages
    const nextLocation = this.props.location; // eslint-disable-line
    const prevLocation = prevProps.location;
    if (nextLocation.key !== prevLocation.key) {
      // scroll to top of a page
      window.scrollTo(0, 0);
      // hide a content
      TweenMax.set(this.wrapper, {
        opacity: 0,
      });

      return true;
    }

    return null;
  }

  setWrapperRef = (b: React.Ref): React.Ref => (this.wrapper = b);

  render(): React.Node {
    return (
      <div ref={this.setWrapperRef}>
        <Helmet>
          <title>Zopa Money Transfer Service</title>
        </Helmet>
        <Switch>
          <Route path="/transfers" exact component={MoneyTransferPage} />
          <Route path="/auth/:method" exact component={AuthPage} />
          <Route path="/logout" exact component={LogoutPage} />
          <Redirect to="/auth/sign-in" />
        </Switch>
      </div>
    );
  }
}

export default withRouter(App);
