import React from 'react';
import injectStyle from 'react-jss';
import type { Classes } from 'react-jss';
import classNames from 'classnames';
import Circle from 'react-circle';
import DataPlaceholder from './DataPlaceholder';
import Paragraph from './Paragraph';
import Box from './Box';
import formatCurrency from '../helpers/formatCurrency';

type Props = {
  classes: Classes,
  className?: string,
  isFetching?: boolean,
  balance: number,
  totalSpent: number,
};

function AccountStatistics(props: Props): React.Node {
  const {
    classes, balance, totalSpent,
    isFetching, className,
  } = props;

  let percent = isFetching ? 100 : 100 - (totalSpent / ((balance + totalSpent) / 100));

  if (isNaN(percent)) {
    percent = 0;
  }

  return (
    <Box
      justify="center"
      align="center"
      className={classNames(classes.wrapper, className)}
    >
      <div className={classes.text}>
        <Paragraph align="right">
          <b>{isFetching ? <DataPlaceholder symbols={8} /> : formatCurrency(totalSpent) }</b>
          <br />
          <span>total sent</span>
        </Paragraph>
      </div>
      <div className={classes.circle}>
        <Circle
          animate
          responsive
          animationDuration="0.45s"
          size={50}
          lineWidth={50}
          progress={percent}
          progressColor="#FFBD2D"
          bgColor="#EDEDF0"
          roundedStroke={false}
          showPercentage={false}
          showPercentageSymbol={false}
        />
      </div>
      <div className={classes.text}>
        <Paragraph align="left">
          <b>{isFetching ? <DataPlaceholder symbols={8} /> : formatCurrency(balance) }</b>
          {' '}
          <br />
          <span>left available</span>
        </Paragraph>
      </div>
    </Box>
  );
}

const styles = {
  wrapper: {
    padding: '70px 0',
  },
  text: {
    width: '30%',
    margin: '0 20px',
  },
  circle: {
    width: '100px',
  },
};

AccountStatistics.defaultProps = {
  className: null,
  isFetching: false,
};

export default injectStyle(styles)(AccountStatistics);
