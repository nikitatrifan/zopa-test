import React, { Component } from 'react';
import injectStyle from 'react-jss';
import type { Classes } from 'react-jss';
import isEmpty from 'lodash/isEmpty';
import type { UserFieldsType } from '../types';
import TextInput from './TextInput';
import Paragraph from './Paragraph';
import Button from './Button';
import Link from './Link';
import parseBalance from '../helpers/parseBalance';
import theme from '../theme';

type Props = {
  classes: Classes,
  isLogin?: boolean,
  onSubmit: Function,
  errorMessage: string,
  isError: boolean,
};

type State = UserFieldsType & {
  errors: UserFieldsType,
};

class AuthForm extends Component<Props, State> {
  static defaultProps = {
    isLogin: false,
  };

  state = {
    name: '',
    email: '',
    password: '',
    balance: 0,
    errors: {
      name: '',
      email: '',
      password: '',
    },
  };

  changeHandler = (e: Event): boolean => {
    const { value, name } = e.target;

    const validationResult = this.validate(name, value);

    this.setState(
      ({ errors }: State): Object => ({
        [name]: name === 'balance' ? parseBalance(value) : value,
        errors: {
          ...errors,
          [name]: validationResult,
        },
      }),
    );
  };

  validate = (name: string, value: string): string => {
    switch (name) {
      case 'balance':
        const balance = parseBalance(value);
        if (isNaN(balance)) {
          return 'This balance is not valid.';
        }
        if (balance < 100) {
          return 'Balance cannot be less than 100£.';
        }
        break;
      case 'name':
        if (isEmpty(value)) {
          return 'Name cannot be empty.';
        }
        break;
      case 'email':
        if (isEmpty(value)) {
          return 'Email cannot be empty.';
        }
        if (value.indexOf('@') === -1 || value.indexOf('.') === -1) {
          return 'This email is not valid.';
        }
        break;
      case 'password':
        if (isEmpty(value)) {
          return 'Password cannot be empty.';
        }
        if (value.length < 6) {
          return 'Password has to be at least 6 symbols.';
        }
        break;
      default:
        break;
    }
    return '';
  };

  submitHandler = (e: Event): boolean => {
    e.preventDefault();

    const { onSubmit } = this.props;
    const { errors, ...fields } = this.state;
    const isValid = !Object.keys(errors).find((prop: string): boolean => !isEmpty(errors[prop]));

    if (!isValid) {
      return false;
    }

    onSubmit(fields);

    return false;
  };

  render(): React.Node {
    const {
      classes, isLogin, isError, errorMessage,
    } = this.props;
    const {
      name, errors, email, password, balance,
    } = this.state;
    return (
      <form onSubmit={this.submitHandler} className={classes.wrapper}>
        {isError && (
          <Paragraph margin="medium" className={classes.errorMessage}>
            {errorMessage}
          </Paragraph>
        )}
        <div className={classes.fields}>
          {!isLogin && (
            <TextInput
              onChange={this.changeHandler}
              name="name"
              placeholder="Name"
              isError={!isEmpty(errors.name)}
              isSuccess={isEmpty(errors.name) && !isEmpty(name)}
              message={errors.name}
              required
            />
          )}
          <TextInput
            onChange={this.changeHandler}
            name="email"
            placeholder="Email address"
            isError={!isEmpty(errors.email)}
            isSuccess={isEmpty(errors.email) && !isEmpty(email)}
            message={errors.email}
            required
          />
          <TextInput
            onChange={this.changeHandler}
            name="password"
            type="password"
            placeholder="Password"
            isError={!isEmpty(errors.password)}
            isSuccess={isEmpty(errors.password) && !isEmpty(password)}
            message={errors.password}
            required
          />
          {!isLogin && (
            <TextInput
              onChange={this.changeHandler}
              name="balance"
              placeholder="Your balance"
              isError={!isEmpty(errors.balance)}
              isSuccess={isEmpty(errors.balance) && !isEmpty(balance)}
              message={errors.balance}
              required
              prefix="£"
            />
          )}
        </div>
        <Button buttonClassName={classes.button} className={classes.buttonWrapper} type="submit">
          {isLogin ? 'Sign In' : 'Sign Up'}
        </Button>
        <Link className={classes.link} to={isLogin ? '/auth/sign-up' : '/auth/sign-in'}>
          {isLogin
            ? "You don't have an account? Sign Up."
            : 'Already have an account? Sign In.'}
        </Link>
      </form>
    );
  }
}

const styles = {
  wrapper: {
    display: 'block',
  },
  button: {
    width: '100%',
  },
  buttonWrapper: {
    width: '100%',
    margin: '20px 0',
  },
  fields: {
    padding: '70px 0 50px',
  },
  link: {
    padding: '20px 0',
    textAlign: 'center',
    display: 'block',
    width: '100%',
  },
  errorMessage: {
    color: theme.redColor,
  },
};

export default injectStyle(styles)(AuthForm);
