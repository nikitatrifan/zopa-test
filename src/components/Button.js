import React, { Component } from 'react';
import classNames from 'classnames';
import { NavLink as RouterLink } from 'react-router-dom';
import { TweenMax } from 'gsap';
import injectStyles from 'react-jss';
import type { Classes } from 'react-jss';
import theme from '../theme';

type Props = {
  classes: Classes,
  className?: string,
  gradient?: Array,
  children: any,
  to?: string,
  buttonClassName?: string,
};

class Button extends Component<Props> {
  static defaultProps = {
    className: '',
    gradient: null,
    to: null,
    buttonClassName: null,
  };

  dur = 0.35;

  scale = 0.85;

  hoverScale = 0.98;

  setRef = (b: React.Ref): React.Ref => (this.button = b);

  hoverAnimation = () => {
    if (!this.button) return;
    TweenMax.to(this.button, this.dur, {
      scale: this.hoverScale,
    });
  };

  startAnimation = () => {
    if (!this.button) return;
    TweenMax.to(this.button, this.dur, {
      scale: this.scale,
    });
  };

  endAnimation = () => {
    if (!this.button) return;
    TweenMax.to(this.button, this.dur, {
      scale: 1,
    });
  };

  render(): React.Node {
    const {
      classes, className, buttonClassName, gradient, children, to, min, ...props
    } = this.props;
    const buttonStyle = gradient && {
      background: `linear-gradient(to right, ${gradient.join(', ')})`,
    };
    const shadowStyle = gradient && {
      backgroundColor: gradient[gradient.length - 1],
    };

    const Wrapper = to ? RouterLink : 'div';

    return (
      <Wrapper className={classNames(classes.wrapper, className)} to={to}>
        <button
          ref={this.setRef}
          onMouseOver={this.hoverAnimation}
          onMouseLeave={this.endAnimation}
          onTouchStart={this.startAnimation}
          onMouseDown={this.startAnimation}
          onTouchEnd={this.endAnimation}
          onMouseUp={this.endAnimation}
          style={buttonStyle}
          type="button"
          {...props}
          className={classNames(classes.button, min && classes.buttonMin, buttonClassName)}
        >
          {children}
        </button>
        <span style={shadowStyle} className={classes.shadow} />
      </Wrapper>
    );
  }
}

const mobileMedia = `@media only screen and (max-width: ${theme.mobilePoint}px)`;
const styles = {
  wrapper: {
    display: 'inline-block',
    position: 'relative',
    zIndex: 0,
    '&:hover span': {
      transform: 'translate(-50%, -40%)',
    },
  },
  button: {
    fontSize: '22px',
    lineHeight: '22px',
    letterSpacing: '0.4px',
    fontFamily: theme.mainFont,
    fontWeight: '600',
    color: '#fff',
    background: `linear-gradient(to right, ${theme.primaryLightColor}, ${theme.primaryColor})`,
    padding: '20px 40px',
    textAlign: 'center',
    borderRadius: '30.5px',
    outline: 'none',
    cursor: 'pointer',
    display: 'inline-block',
    position: 'relative',
    userSelect: 'none',
    zIndex: 10,
    border: 'none',
    // transition: 'transform .25s ease-in-out',
    whiteSpace: 'pre',
    [mobileMedia]: {
      fontSize: '18px',
      lineHeight: '18px',
    },
  },
  buttonMin: {
    fontSize: '16px',
    lineHeight: '16px',
    padding: '15px 30px',
  },
  shadow: {
    display: 'block',
    position: 'absolute',
    left: '50%',
    top: '70%',
    width: '70%',
    height: '100%',
    transform: 'translate(-50%, -50%)',
    backgroundColor: theme.primaryColor,
    zIndex: 0,
    borderRadius: '30.5px',
    filter: 'blur(20px)',
    opacity: 0.35,
    transition: 'transform .25s ease-in-out',
  },
};

export default injectStyles(styles)(Button);
