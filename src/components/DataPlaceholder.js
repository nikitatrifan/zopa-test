import React, { Component } from 'react';
import classNames from 'classnames';
import injectStyle from 'react-jss';
import type { Classes } from 'react-jss';
import { times, constant } from 'lodash';
import { TweenMax } from 'gsap';

type Props = {
  className?: string,
  fullSize?: boolean,
  width?: string,
  height?: string,
  textLines?: number,
  symbols?: number,
  classes?: Classes
};

class DataPlaceholder extends Component<Props> {
  static defaultProps = {
    width: '100%',
    height: 'auto',
    textLines: 1,
    fullSize: false,
    symbols: 10,
    className: null,
  };

  placeholders = [];

  dur = 1;

  ease = 'Cubic.easeOut';

  componentDidMount() {
    this.animate();
  }

  componentWillUnmount() {
    this.isUnMounted = true;
  }

  animate = () => {
    if (this.isUnMounted) {
      return;
    }

    const { dur, ease, placeholders } = this;
    TweenMax.staggerFromTo(placeholders, dur, {
      left: '-100%',
      opacity: .8,
    }, {
      left: '100%',
      opacity: 1,
      ease,
    }, .04, this.animate);
  };

  render(): React.Node {
    const {
      classes, className, width,
      height, textLines, symbols,
    } = this.props;
    const lines = times(textLines, constant(null));
    const symbolsArr = times(symbols, constant(null));
    return (
      <span style={{ width }} className={classNames(classes.wrapper, className)}>
        {lines.map((it: null, key: number): React.Node => (
          <span key={key} className={classes.item} style={{ height }}>
            <span
              ref={(b: React.Ref): React.Ref => (this.placeholders[key] = b)}
              className={classes.placeholder}
            />
            {symbolsArr.map((): string => '1').join('')}
          </span>
        ))}
      </span>
    );
  }
}

const fullSizeStyle = {
  position: 'absolute',
  display: 'block',
  left: 0, top: 0,
  width: '100%', height: '100% !important'
};

const styles = {
  wrapper: (props: Props) => ({
    display: 'inline-block',
    fontSize: 'inherit',
    lineHeight: 'inherit',
    ...(props.fullSize ? fullSizeStyle : {}),
    opacity: props.opacity || 1,
  }),
  item: (props: Props) => ({
    backgroundColor: '#f0f0f0',
    color: 'transparent',
    display: 'inline-block',
    position: 'relative',
    overflow: 'hidden',
    ...(props.fullSize ? fullSizeStyle : {}),
  }),
  placeholder: {
    position: 'absolute',
    left: 0, top: 0,
    width: '100%',
    height: '100%',
    background: 'linear-gradient(to right, #f0f0f0 0%, #fff 50%, #f0f0f0 100%)',
    willChange: 'left, opacity',
  },
};

export default injectStyle(styles)(DataPlaceholder);
