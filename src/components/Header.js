import React from 'react';
import injectStyle from 'react-jss';
import type { Classes } from 'react-jss';
import classNames from 'classnames';
import Container from './Container';
import Paragraph from './Paragraph';
import Box from './Box';
import Link from './Link';
import theme from '../theme';

export type Props = {
  classes: Classes,
  className?: string,
  userName: string,
};

function Header(props: Props): React.Node {
  const {
    classes, userName, className,
  } = props;
  return (
    <div className={classNames(classes.wrapper, className)}>
      <Container>
        <Box justify="between" align="center">
          <div className={classes.col}>
              <Paragraph id="user-name">
                Hello,
                {' '}
                {userName}
              </Paragraph>
          </div>
          <div className={classes.col}>
            <Box justify="between" align="center">
              <Link
                className={classes.logout}
                color={theme.redColor}
                to="/logout"
              >
                Log Out
              </Link>
            </Box>
          </div>
        </Box>
      </Container>
    </div>
  );
}

const styles = {
  wrapper: {
    position: 'fixed',
    left: 0,
    top: 0,
    width: '100%',
    padding: '30px 0',
  },
  userName: {
    marginRight: '15px',
  },
  logout: {
    textTransform: 'uppercase',
  },
};

Header.defaultProps = {
  className: null,
};

export default injectStyle(styles)(Header);
