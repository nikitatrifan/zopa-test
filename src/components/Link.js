import React from 'react';
import classNames from 'classnames';
import { Link as RouterLink } from 'react-router-dom';
import injectStyles from 'react-jss';
import theme from '../theme';

type Props = {
  icon: boolean,
  to: string,
  color?: string,
  className: string,
  children: Any,
};

const Link = ({
  classes, to, color, className, children, ...props
}: Props): React.Node => {
  const Route = to.indexOf('http') !== -1 ? 'a' : RouterLink;
  const linkData = Route === 'a' ? { href: to } : { to };
  return (
    <Route
      style={{ color }}
      className={classNames(classes.link, className)}
      {...linkData}
      {...props}
    >
      {children}
    </Route>
  );
};

const styles = {
  link: {
    display: 'inline-block',
    outline: 'none',
    textDecoration: 'none',
    cursor: 'pointer',
    color: theme.textColor,
    transition: 'opacity .3s ease-in-out',
    '&:hover': {
      opacity: 0.6,
    },
  },
  wrapper: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
};

Link.defaultProps = {
  color: theme.textColor,
};

export default injectStyles(styles)(Link);
