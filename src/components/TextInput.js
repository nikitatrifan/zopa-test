import React, { Component } from 'react';
import injectStyle from 'react-jss';
import type { Classes } from 'react-jss';
import classNames from 'classnames';
import isEmpty from 'lodash/isEmpty';
import ComponentFadeIn from './ComponentFadeIn';
import theme from '../theme';

type Props = {
  classes: Classes,
  onChange: Function,
  placeholder: string,
  isError?: boolean,
  message?: string,
  className?: string,
  isSuccess?: boolean,
  type?: string,
  prefix?: string,
  required?: boolean,
};

type State = {
  isFocused: boolean,
  value: string,
};

class TextInput extends Component<Props, State> {
  static defaultProps = {
    isError: false,
    message: null,
    className: null,
    isSuccess: false,
    prefix: '',
    type: 'text',
    required: false,
  };

  static inputPaddingVertical = 14;

  state = {
    isFocused: false,
    value: '',
  };

  handleChange = (e: Event): boolean => {
    const { onChange, prefix } = this.props;
    const { value } = e.target;

    this.setState({ value });
    onChange(e);

    if (!isEmpty(prefix) && value.indexOf(prefix) === -1) {
      e.target.value = `${prefix} ${value}`;
    }

    return false;
  };

  toggleFocused = () => {
    this.setState(
      ({ isFocused }: State): Object => ({
        isFocused: !isFocused,
      }),
    );
  };

  render(): React.Node {
    const {
      classes,
      className,
      placeholder,
      isError,
      isSuccess,
      message,
      type,
      required,
      ...restProps
    } = this.props;
    const { isFocused, value } = this.state;
    const wrapperClassName = classNames(classes.wrapper, className);
    const messageClassName = classNames(
      classes.message,
      isError && classes.messageError,
      isSuccess && classes.messageSuccess,
    );
    const inputClassName = classNames(
      classes.input,
      isError && classes.inputError,
      (isFocused && !isError) && classes.inputFocus,
    );
    const placeholderClassName = classNames(
      classes.placeholder,
      // if user filed something in input we don't want to
      // replace his value to a placeholder
      (isFocused || !isEmpty(value) || isError) && classes.placeholderActive,
    );

    return (
      <div className={wrapperClassName}>
        <span className={placeholderClassName}>{placeholder}</span>
        <input
          {...restProps}
          className={inputClassName}
          type={type}
          onFocus={this.toggleFocused}
          onBlur={this.toggleFocused}
          onChange={this.handleChange}
          required={required}
        />
        {!isEmpty(message) && (
          <ComponentFadeIn gap={0}>
            <span className={messageClassName}>{message}</span>
          </ComponentFadeIn>
        )}
      </div>
    );
  }
}

const styles = {
  wrapper: {
    display: 'block',
    position: 'relative',
    zIndex: 0,
    // an offset for our placeholder
    // as we know line-height is 28px
    marginTop: '58px',
    marginBottom: '38px',
  },
  input: {
    display: 'block',
    boxSizing: 'border-box',
    width: '100%',
    boxShadow: 'none',
    border: 'none',
    borderBottom: '2px solid #DBDCE2',
    outline: 'none',
    background: 'transparent',
    padding: `${TextInput.inputPaddingVertical}px 0px`,
    fontSize: '21px',
    lineHeight: '28px',
    fontFamily: theme.mainFont,
    position: 'relative',
    zIndex: 10,
  },
  inputFocus: {
    borderBottomColor: theme.primaryColor,
  },
  inputError: {
    borderBottomColor: theme.redColor,
  },
  placeholder: {
    userSelect: 'none',
    display: 'block',
    position: 'absolute',
    left: 0,
    zIndex: 0,
    top: `${TextInput.inputPaddingVertical}px`,
    fontSize: '21px',
    lineHeight: '28px',
    fontFamily: theme.mainFont,
    willChange: 'opacity, top, transform, font-size',
    transition: [
      'opacity .15s ease-in-out',
      'top .15s ease-in-out',
      'transform .15s ease-in-out',
      'font-size .15s ease-in-out',
    ].join(','),
  },
  placeholderActive: {
    top: 0,
    transform: 'translateY(-100%)',
    opacity: 0.5,
    fontSize: '18px',
  },
  message: {
    fontSize: '18px',
    lineHeight: '18px',
    padding: '4px 0',
    display: 'block',
    position: 'absolute',
    left: 0,
    bottom: '-28px',
    width: '100%',
  },
  messageSuccess: {
    color: theme.primaryColor,
  },
  messageError: {
    color: theme.redColor,
  },
};

export default injectStyle(styles)(TextInput);
