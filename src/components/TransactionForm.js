import React, { Component } from 'react';
import injectStyle from 'react-jss';
import type { Classes } from 'react-jss';
import isEmpty from 'lodash/isEmpty';
import TextInput from './TextInput';
import Paragraph from './Paragraph';
import Button from './Button';
import theme from '../theme';

type Props = {
  classes: Classes,
  onSubmit: Function,
  errorMessage: string,
  isError: boolean,
  disabled?: boolean,
};

type State = {
  name: string,
  email: string,
  amount: string,
  errors: {
    name: string,
    email: string,
    amount: string,
  },
};

class TransactionForm extends Component<Props, State> {
  static defaultProps = {
    disabled: false,
  };

  state = {
    name: '',
    email: '',
    amount: '',
    errors: {
      name: '',
      email: '',
      amount: '',
    },
  };

  changeHandler = (e: Event): boolean => {
    const { value, name } = e.target;

    const validationResult = this.validate(name, value);

    this.setState(
      ({ errors }: State): Object => ({
        [name]: value,
        errors: {
          ...errors,
          [name]: validationResult,
        },
      }),
    );
  };

  validate = (name: string, value: string): string => {
    switch (name) {
      case 'name':
        if (isEmpty(value)) {
          return 'Name cannot be empty.';
        }
        break;
      case 'email':
        if (isEmpty(value)) {
          return 'Email cannot be empty.';
        }
        if (value.indexOf('@') === -1 || value.indexOf('.') === -1) {
          return 'This email is not valid.';
        }
        break;
      case 'amount':
        const parsedAmount = parseFloat(value.replace('£', ''));
        if (isEmpty(value)) {
          return 'Amount cannot be empty.';
        }
        if (isNaN(parsedAmount) || !parsedAmount) {
          return 'This amount is not valid.';
        }
        if (parsedAmount < 100) {
          return 'Amount has to be at least more than 100.';
        }
        break;
      default:
        return '';
    }
  };

  submitHandler = (e: Event): boolean => {
    e.preventDefault();

    const { onSubmit, disabled } = this.props;
    const { errors, ...fields } = this.state;

    if (disabled) {
      return false;
    }

    const isValid = !Object.keys(errors).find((prop: string): boolean => !isEmpty(errors[prop]));

    if (!isValid) {
      return false;
    }

    onSubmit(fields);

    return false;
  };

  render(): React.Node {
    const { classes, isError, errorMessage } = this.props;
    const {
      errors, name, email, amount,
    } = this.state;
    return (
      <form onSubmit={this.submitHandler} className={classes.wrapper}>
        {isError && (
          <Paragraph margin="medium" className={classes.errorMessage}>
            {errorMessage}
          </Paragraph>
        )}
        <div className={classes.fields}>
          <TextInput
            onChange={this.changeHandler}
            name="name"
            placeholder="Name"
            isError={!isEmpty(errors.name)}
            isSuccess={isEmpty(errors.name) && !isEmpty(name)}
            message={errors.name}
            required
          />
          <TextInput
            onChange={this.changeHandler}
            name="email"
            placeholder="Email address"
            isError={!isEmpty(errors.email)}
            isSuccess={isEmpty(errors.email) && !isEmpty(email)}
            message={errors.email}
            required
          />
          <TextInput
            onChange={this.changeHandler}
            name="amount"
            placeholder="Amount"
            isError={!isEmpty(errors.amount)}
            isSuccess={isEmpty(errors.amount) && !isEmpty(amount)}
            message={errors.amount}
            prefix="£"
            required
          />
        </div>
        <Button buttonClassName={classes.button} className={classes.buttonWrapper} type="submit">
          Send
        </Button>
      </form>
    );
  }
}

const styles = {
  wrapper: {
    display: 'block',
    marginTop: '30px',
  },
  button: {
    width: '100%',
  },
  buttonWrapper: {
    width: '100%',
    margin: '20px 0',
  },
  fields: {
    padding: '0 0 50px',
  },
  link: {
    padding: '20px 0',
    textAlign: 'center',
    display: 'block',
    width: '100%',
  },
  errorMessage: {
    color: theme.redColor,
  },
};

export default injectStyle(styles)(TransactionForm);
