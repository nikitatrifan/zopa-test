import React from 'react';
import injectStyle from 'react-jss';
import type { Classes } from 'react-jss';
import classNames from 'classnames';
import type { TransactionFieldsType } from '../types';
import DataPlaceholder from './DataPlaceholder';
import ComponentFadeIn from './ComponentFadeIn';
import Box from './Box';
import formatCurrency from '../helpers/formatCurrency';

type Props = {
  classes?: Classes,
  className?: string,
  data?: TransactionFieldsType,
  isFetching?: boolean,
};

function TransactionRowItem(props: Props): React.Node {
  const {
    classes, data, isFetching, className,
  } = props;
  return (
    <ComponentFadeIn duration={0.35} gap={0}>
      <Box justify="between" align="stretch" className={classNames(classes.wrapper, className)}>
        <div className={classes.col}>
          <span className={classes.name}>
            {isFetching ? <DataPlaceholder symbols={7} /> : data.recipientName}
          </span>
          <span className={classes.email}>
            {isFetching ? <DataPlaceholder symbols={15} /> : data.recipientEmail}
          </span>
        </div>
        <div className={classes.col}>
          <b className={classes.amount}>
            {isFetching ? <DataPlaceholder symbols={9} /> : formatCurrency(data.amount)}
          </b>
        </div>
      </Box>
    </ComponentFadeIn>
  );
}

const styles = {
  wrapper: {
    padding: '12px 0',
    borderBottom: '1px solid #ececec',
  },
  col: {
    width: 'auto',
  },
  name: {
    display: 'block',
    color: '#383D56',
    fontSize: '21px',
    lineHeight: 1,
  },
  email: {
    display: 'block',
    color: '#575757',
    fontSize: '16px',
    lineHeight: 1,
    paddingTop: '9px',
  },
  amount: {
    display: 'block',
    color: '#222844',
    fontSize: '21px',
    lineHeight: 1,
  },
};

TransactionRowItem.defaultProps = {
  classes: {},
  className: null,
  data: {},
  isFetching: false,
};

export default injectStyle(styles)(TransactionRowItem);
