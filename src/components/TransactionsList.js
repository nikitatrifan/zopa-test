import React, { Component } from 'react';
import injectStyle from 'react-jss';
import type { Classes } from 'react-jss';
import classNames from 'classnames';
import isEmpty from 'lodash/isEmpty';
import { TweenMax } from 'gsap';
import Heading from './Heading';
import TransactionRowItem from './TransactionRowItem';
import Paragraph from './Paragraph';
import type { TransactionFieldsType } from '../types';
import responsive from '../helpers/responsive';

type Props = {
  classes?: Classes,
  className?: string,
  isFetching?: boolean,
  isFetchingForFirst?: boolean,
  data: Array<TransactionFieldsType>,
};

const fetchingLines = [0, 1, 2, 3, 4];

class TransactionsList extends Component<Props> {
  static defaultProps = {
    className: null,
    isFetching: true,
    isFetchingForFirst: true,
    classes: {},
  };

  maxDataLength = 5;

  componentDidMount() {
    this.updateScroller();
  }

  componentDidUpdate() {
    this.updateScroller();
  }

  updateScroller = () => {
    if (responsive().isMobile) {
      return;
    }

    const { data } = this.props;

    const isScrollable = !isEmpty(data) && data.length > this.maxDataLength;

    if (isScrollable) {
      const scrollerHeight = this.scroller.firstChild.clientHeight;
      TweenMax.to(this.scroller, 0.3, {
        scrollTo: { y: scrollerHeight },
        ease: 'Cubic.easeOut',
      });
    }
  };

  scrollableRef = (b: React.Ref): React.Ref => (this.scroller = b);

  render(): React.Node {
    const {
      classes, data, isFetching, className,
      isFetchingForFirst,
    } = this.props;

    const isDataEmpty = isEmpty(data);
    const isScrollable = !isDataEmpty && data.length > this.maxDataLength;

    return (
      <div className={classNames(classes.wrapper, className)}>
        <Heading size="3">
          Transactions
        </Heading>
        <div
          ref={this.scrollableRef}
          className={classNames(classes.transactions, isScrollable && classes.scrollable)}
        >
          <div className={classes.scroller}>
            {/* if it is initial fetch of transactions */}
            {isDataEmpty && isFetching && isFetchingForFirst && (
              fetchingLines.map((it: number): React.Node => (
                <TransactionRowItem isFetching key={it} />
              ))
            )}
            {/* user transactions list */}
            {!isDataEmpty && data.map((it: Object, key: number): React.Node => (
              <TransactionRowItem data={it} key={key} />
            ))}
            {/* if user has transactions and he just send new one */}
            {!isDataEmpty && isFetching && (
              <TransactionRowItem isFetching />
            )}
            {/* if user has no transactions and he just send new one */}
            {isDataEmpty && !isFetchingForFirst && isFetching && (
              <TransactionRowItem isFetching />
            )}
            {/* if there is no any transactions */}
            {isDataEmpty && !isFetching && (
              <Paragraph margin="medium">
                Make your first transaction!
              </Paragraph>
            )}
          </div>
        </div>
      </div>
    );
  }
}
const styles = {
  wrapper: {
    display: 'block',
  },
  transactions: {
    marginTop: '30px',
  },
  scrollable: {
    minHeight: '300px',
    maxHeight: '40vh',
    overflowY: 'scroll',
    [responsive('mobile')]: {
      minHeight: 'auto',
      maxHeight: 'none',
      overflowY: 'auto',
    },
  },
};

export default injectStyle(styles)(TransactionsList);
