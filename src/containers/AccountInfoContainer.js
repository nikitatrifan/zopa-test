import React, { Component } from 'react';
import { connect } from 'react-redux';
import type { TransactionFieldsType } from '../types';
import TransactionsList from '../components/TransactionsList';
import { fetchTransactions } from '../store/actions/transactionsActions';
import { fetchUserBalance } from '../store/actions/userActions';
import AccountStatistics from '../components/AccountStatistics';

type Props = {
  className?: string,
  isFetching?: boolean,
  data?: Array<TransactionFieldsType>,
  fetchData: Function,
  balance?: number,
  transactionsIncome?: number,
  updateBalance: Function,
};

class AccountInfoContainer extends Component<Props> {
  static defaultProps = {
    className: null,
    isFetching: false,
    balance: 0,
    transactionsIncome: 0,
    data: [],
  };

  componentDidMount() {
    const { fetchData, updateBalance } = this.props;
    fetchData();
    updateBalance();
  }

  componentDidUpdate(prevProps: Props) {
    if (!this.props.isFetching && prevProps.isFetching) {
      this.passFirstFetching = true;
    }
  }

  getTotalSpent = (data: Array<TransactionFieldsType>): number => {
    let totalSpent = 0;
    data.forEach((transaction: TransactionFieldsType) => {
      totalSpent += transaction.amount;
    });
    return totalSpent;
  };

  render(): React.Node {
    const {
      data, balance, transactionsIncome, isFetching, className,
    } = this.props;

    const totalSpent = this.getTotalSpent(data);

    return (
      <div className={className}>
        <AccountStatistics
          balance={balance + transactionsIncome}
          totalSpent={totalSpent}
          isFetching={isFetching}
        />
        <TransactionsList
          data={data}
          isFetchingForFirst={!this.passFirstFetching}
          isFetching={isFetching}
        />
      </div>
    );
  }
}

const mapStateToProps = ({
  transactions: { data, isFetching },
  user: { balance, transactionsIncome },
}: Object): Props => ({
  data,
  isFetching,
  balance,
  transactionsIncome,
});

const mapDispatchToProps = {
  fetchData: fetchTransactions,
  updateBalance: fetchUserBalance,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AccountInfoContainer);
