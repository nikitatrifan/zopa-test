import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loading from 'react-loading-bar';
import type { UserState } from '../store/reducers/user';
import AuthForm from '../components/AuthForm';
import type { Props as AuthFormProps } from '../components/AuthForm';
import type { UserFieldsType } from '../types';
import { userLogin, userSignup } from '../store/actions/userActions';
import theme from '../theme';

type Props = {
  isError: boolean,
  isFetching: boolean,
  errorMessage: string,
};

class AuthFormContainer extends Component<Props & AuthFormProps> {
  submitHandler = (fields: UserFieldsType) => {
    const {
      isFetching, isLogin, login, signup,
    } = this.props;

    if (isFetching) return;

    if (isLogin) {
      login(fields);
      return;
    }

    signup(fields);
  };

  render(): React.Node {
    const {
      isError, isLogin, isFetching, errorMessage,
    } = this.props;
    return (
      <div>
        <AuthForm
          isLogin={isLogin}
          onSubmit={this.submitHandler}
          isError={isError}
          errorMessage={errorMessage}
          disabled={isFetching}
        />
        <Loading
          show={isFetching}
          color={theme.primaryColor}
          showSpinner={false}
        />
      </div>
    );
  }
}

const mapStateToProps = ({ user: { isFetching, isError, errorMessage } }: UserState): Object => ({
  isFetching,
  isError,
  errorMessage,
});

const mapDispatchToProps = {
  login: userLogin,
  signup: userSignup,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AuthFormContainer);
