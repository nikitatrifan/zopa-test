import React from 'react';
import { connect } from 'react-redux';
import Header from '../components/Header';
import type { Props as HeaderProps } from '../components/Header';

type Props = HeaderProps & {
  userName: string,
  onLogout: Function,
};

function HeaderContainer(props: Props): React.Node {
  return <Header {...props} />;
}

const mapStateToProps = ({ user: { name } }: Object): Object => ({
  userName: name,
});

export default connect(mapStateToProps)(HeaderContainer);
