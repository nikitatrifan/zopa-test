import React, { Component } from 'react';
import { connect } from 'react-redux';
import TransactionForm from '../components/TransactionForm';
import { createTransaction } from '../store/actions/transactionsActions';

type Props = {
  className?: string,
  isFetching?: boolean,
  onSubmit: Function,
  errorMessage?: string,
  isError?: boolean,
};

class TransactionFormContainer extends Component<Props> {
  static defaultProps = {
    className: null,
    isFetching: false,
    isError: false,
    errorMessage: null,
  };

  submitHandler = (fields: Object) => {
    const { onSubmit } = this.props;

    onSubmit({
      recipientEmail: fields.email,
      recipientName: fields.name,
      amount: parseFloat(fields.amount.replace('£ ', '')),
    });
  };

  render(): React.Node {
    const {
      className, errorMessage,
      isError, isFetching,
    } = this.props;
    return (
      <TransactionForm
        errorMessage={errorMessage}
        isError={isError}
        onSubmit={this.submitHandler}
        disabled={isFetching}
        className={className}
      />
    );
  }
}

const mapStateToProps = ({
  transactions: { isFetching, errorMessage, isError },
}: Object): Props => ({
  isFetching, errorMessage, isError,
});
const mapDispatchToProps = {
  onSubmit: createTransaction,
};

export default connect(mapStateToProps, mapDispatchToProps)(TransactionFormContainer);
