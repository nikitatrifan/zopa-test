export default (value: string | number): string => {
  const amount = parseFloat(value)
    .toFixed(2)
    .replace(/\d(?=(\d{3})+\.)/g, '$&,');

  return `£${amount}`;
};
