export default function parseBalance(val: string): number {
  return parseFloat(val.replace('£', ''));
}
