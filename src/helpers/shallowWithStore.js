import React from 'react';
import { shallow } from 'enzyme/build';

const shallowWithStore = (component: any, store: Object): Function => {
  const context = {
    store,
  };

  return shallow(component, { context });
};

export default shallowWithStore;
