import './index.css';
import 'react-loading-bar/dist/index.css';
import 'gsap/ScrollToPlugin';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import App from './App';
import { store } from './store';
import registerServiceWorker from './registerServiceWorker';

firebase.initializeApp({
  apiKey: 'AIzaSyAeHw8X34ukSNACkfMUD0lEMJH0GpraN7c',
  authDomain: 'zopa-c6adf.firebaseapp.com',
  databaseURL: 'https://zopa-c6adf.firebaseio.com',
  projectId: 'zopa-c6adf',
  storageBucket: 'zopa-c6adf.appspot.com',
  messagingSenderId: '476251644231',
});

render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root'),
);

if (module.hot) {
  module.hot.accept(() => {
    // eslint-disable-next-line
    const NewApp = require('./App.js').default;
    // eslint-disable-next-line
    const NewStore = require('./store/index.js').store;

    render(
      <Provider store={NewStore}>
        <BrowserRouter>
          <NewApp />
        </BrowserRouter>
      </Provider>,
      document.getElementById('root'),
    );
  });
}

registerServiceWorker();
