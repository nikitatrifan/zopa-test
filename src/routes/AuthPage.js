import React from 'react';
import injectStyle from 'react-jss';
import type { Classes } from 'react-jss';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import type { UserState } from '../store/reducers/user';
import Container from '../components/Container';
import Heading from '../components/Heading';
import Paragraph from '../components/Paragraph';
import Box from '../components/Box';
import AuthFormContainer from '../containers/AuthFormContainer';

type Props = {
  classes: Classes,
  isAuthenticated: boolean,
  match: {
    params: { method: string },
  },
};

function AuthPage(props: Props): React.Node {
  const {
    classes,
    isAuthenticated,
    match: {
      params: { method },
    },
  } = props;

  if (isAuthenticated) {
    return <Redirect to="/transfers" />;
  }

  // in case we cannot recognize the route.
  if (method.indexOf('sign-in') === -1 && method.indexOf('sign-up') === -1) {
    return <Redirect to="/" />;
  }

  const isLogin = method.indexOf('sign-in') !== -1;

  return (
    <Box justify="center" align="center" direction="column" className={classes.wrapper}>
      <Container className={classes.container} type="content">
        <Heading margin="medium">
          Welcome to the
          {' '}
          <br />
          Zopa money transfer.
        </Heading>
        <Paragraph margin="small">
          {isLogin
            ? 'Please, sign in to your account to start sending money'
            : 'Please, sign up to our service to use all the benefits.'}
        </Paragraph>
        <AuthFormContainer isLogin={isLogin} />
      </Container>
    </Box>
  );
}

const styles = {
  wrapper: {
    backgroundColor: '#fafafa',
    padding: '60px 0',
    minHeight: '100vh',
  },
  container: {
    maxWidth: '560px',
  },
};

const mapStateToProps = ({ user: { isAuthenticated } }: UserState): Object => ({
  isAuthenticated,
});

export default connect(mapStateToProps)(injectStyle(styles)(AuthPage));
