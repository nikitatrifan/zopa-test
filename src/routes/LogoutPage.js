import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { userLogout } from '../store/actions/userActions';

type Props = {
  logoutHandler: Function,
};

class LogoutPage extends Component<Props> {
  componentDidMount() {
    const { logoutHandler } = this.props;

    logoutHandler();
  }

  render(): React.Node {
    return <Redirect to="/auth/sign-in" />;
  }
}

const mapDispatchToProps = { logoutHandler: userLogout };

export default connect(null, mapDispatchToProps)(LogoutPage);
