import React from 'react';
import injectStyle from 'react-jss';
import type { Classes } from 'react-jss';
import { Redirect } from 'react-router-dom';
import classNames from 'classnames';
import { connect } from 'react-redux';
import Loading from 'react-loading-bar';
import AccountInfoContainer from '../containers/AccountInfoContainer';
import TransactionFormContainer from '../containers/TransactionFormContainer';
import HeaderContainer from '../containers/HeaderContainer';
import Container from '../components/Container';
import Heading from '../components/Heading';
import Box from '../components/Box';
import theme from '../theme';
import responsive from '../helpers/responsive';

type Props = {
  classes: Classes,
  className?: string,
  isFetching: boolean,
  isAuthenticated?: boolean,
};

function MoneyTransferPage(props: Props): React.Node {
  const {
    classes, isFetching, isAuthenticated, className,
  } = props;

  if (!isAuthenticated) {
    return <Redirect to="/auth/sign-in" />;
  }

  return (
    <Box
      justify="center"
      align="center"
      direction="column"
      className={classNames(classes.wrapper, className)}
    >
      <HeaderContainer />
      <Container type="bootstrap">
        <Box className={classes.grid} wrap justify="between" align="start">
          <div className={classes.col}>
            <Heading>
              Send money
            </Heading>
            <TransactionFormContainer />
          </div>
          <div className={classes.col}>
            <Heading>
              My account
            </Heading>
            <AccountInfoContainer />
          </div>
        </Box>
      </Container>
      <Loading
        show={isFetching}
        color={theme.primaryColor}
        showSpinner={false}
      />
    </Box>
  );
}

const styles = {
  wrapper: {
    minHeight: '100vh',
    padding: '150px 0 100px',
  },
  col: {
    width: '42%',
    [responsive('mobile')]: {
      width: '100%',
      padding: '20px 0',
    },
  },
  grid: {
    [responsive('mobile')]: {
      flexDirection: 'column-reverse',
    },
  },
};

MoneyTransferPage.defaultProps = {
  className: null,
  isFetching: false,
  isAuthenticated: false,
};

const mapStateToProps = ({ user: { isAuthenticated }, transactions: { isFetching } }: Object): Props => ({
  isAuthenticated, isFetching,
});

export default connect(mapStateToProps)(injectStyle(styles)(MoneyTransferPage));
