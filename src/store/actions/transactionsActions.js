import firebase from 'firebase/app';
import {
  FETCH_TRANSACTIONS_REQUEST,
  FETCH_TRANSACTIONS_SUCCESS,
  FETCH_TRANSACTIONS_FAILURE,
  SEND_TRANSACTION_REQUEST,
  SEND_TRANSACTION_SUCCESS,
  SEND_TRANSACTION_FAILURE,
} from '../constants/transactionsConstants';
import type { TransactionFieldsType } from '../../types';
import { USER_SYNC_DATA } from '../constants/userConstants';

export const fetchTransactions = (): Function => async (
  dispatch: Function, getState: Function,
): any => {
  dispatch({ type: FETCH_TRANSACTIONS_REQUEST });
  try {
    // set up the firestore
    const db = firebase.firestore();
    const settings = { timestampsInSnapshots: true };
    db.settings(settings);
    // fetch our data
    const userEmail = getState().user.email;
    const result = await db.collection('transactions').where('senderEmail', '==', userEmail).get();
    // parse our data
    const data = [];
    result.forEach((doc: Object) => {
      const transaction = doc.data();
      // firestore requirement
      transaction.createdAt = transaction.createdAt.toDate();
      data.push(transaction);
    });
    dispatch({ type: FETCH_TRANSACTIONS_SUCCESS, payload: data });
  } catch (e) {
    dispatch({ type: FETCH_TRANSACTIONS_FAILURE, payload: e.message });
  }
};

export const createTransaction = (transaction: TransactionFieldsType): Function => async (
  dispatch: Function, getState: Function,
): any => {
  dispatch({ type: SEND_TRANSACTION_REQUEST });

  try {
    // set up the firestore
    const db = firebase.firestore();
    const settings = { timestampsInSnapshots: true };
    db.settings(settings);
    // send our data to a server
    const {
      email, name, balance,
      transactionsIncome = 0,
    } = getState().user;
    const userBalance = balance + transactionsIncome;
    const transactionAmount = parseFloat(transaction.amount);
    // user must have enough money to send a transaction
    if (userBalance < transactionAmount) {
      const errorMessage = 'You have not enough money on your account to send this transaction.';
      const error = new Error(errorMessage);
      error.message = errorMessage;
      throw error;
    }
    // user can not send his to money to his account
    if (email === transaction.recipientEmail) {
      const errorMessage = 'You can not send money to your account.';
      const error = new Error(errorMessage);
      error.message = errorMessage;
      throw error;
    }

    const newTransaction = {
      ...transaction,
      amount: transactionAmount,
      senderEmail: email,
      createdAt: new Date(),
    };
    await db.collection('transactions').add(newTransaction);
    // now we have to update user balance
    const { currentUser } = firebase.auth();
    // firebase doesn't support any custom fields for user
    // so we will store our data in displayName prop.
    const newUserBalance = balance - transactionAmount;
    await currentUser.updateProfile({
      displayName: JSON.stringify({
        name, balance: newUserBalance,
      }),
    });

    dispatch({ type: SEND_TRANSACTION_SUCCESS, payload: newTransaction });
    dispatch({ type: USER_SYNC_DATA, payload: { balance: newUserBalance } });
  } catch (e) {
    dispatch({ type: SEND_TRANSACTION_FAILURE, payload: e.message });
  }
};
