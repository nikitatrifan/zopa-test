import firebase from 'firebase/app';
import {
  USER_SYNC_DATA,
  USER_FETCH_BALANCE,
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE,
  USER_SIGNUP_REQUEST,
  USER_SIGNUP_SUCCESS,
  USER_SIGNUP_FAILURE,
  USER_LOGOUT_REQUEST,
  USER_LOGOUT_SUCCESS,
  USER_LOGOUT_FAILURE,
} from '../constants/userConstants';
import type { UserFieldsType } from '../../types';

// user login
export const userLogin = (fields: UserFieldsType): Function => async (dispatch: Function): any => {
  dispatch({ type: USER_LOGIN_REQUEST, payload: fields });

  try {
    const { email, password } = fields;
    const auth = firebase.auth();
    // the only way to get all user's data
    // is to subscribe to a listener and do the thing
    auth.onAuthStateChanged((user: Object) => {
      if (user) {
        const userData = JSON.parse(user.displayName) || {};
        const data = {
          balance: userData.balance || fields.balance,
          name: userData.name || fields.name,
        };

        dispatch({ type: USER_SYNC_DATA, payload: data });
      }
    });

    await auth.signInWithEmailAndPassword(email, password);
    dispatch({ type: USER_LOGIN_SUCCESS });
  } catch (e) {
    dispatch({ type: USER_LOGIN_FAILURE, payload: e.message });
  }
};

// user sign up actions
export const userSignup = (fields: UserFieldsType): Function => async (dispatch: Function): any => {
  dispatch({ type: USER_SIGNUP_REQUEST, payload: fields });

  try {
    const {
      email, password, balance, name,
    } = fields;
    const auth = firebase.auth();

    await auth.createUserWithEmailAndPassword(email, password);
    const { currentUser } = firebase.auth();
    // firebase doesn't support any custom fields for user
    // so we will store our data in displayName prop.
    await currentUser.updateProfile({
      displayName: JSON.stringify({ name, balance }),
    });

    dispatch({ type: USER_SIGNUP_SUCCESS });
    dispatch({ type: USER_SYNC_DATA, payload: { balance, name } });
  } catch (e) {
    dispatch({ type: USER_SIGNUP_FAILURE, payload: e.message });
  }
};
export const userLogout = (): Function => async (dispatch: Function): any => {
  dispatch({ type: USER_LOGOUT_REQUEST });

  try {
    const auth = firebase.auth();
    await auth.signOut();
    dispatch({ type: USER_LOGOUT_SUCCESS });
  } catch (e) {
    dispatch({ type: USER_LOGOUT_FAILURE, payload: e.message });
  }
};

export const fetchUserBalance = (): Function => async (
  dispatch: Function, getState: Function,
): any => {
  dispatch({ type: USER_FETCH_BALANCE });

  try {
    // set up the firestore
    const db = firebase.firestore();
    const settings = { timestampsInSnapshots: true };
    db.settings(settings);
    // fetch our data
    const { email } = getState().user;
    const result = await db.collection('transactions').where('recipientEmail', '==', email).get();
    // parse our data
    let transactionsIncome = 0;
    result.forEach((doc: Object) => {
      const transaction = doc.data();
      if (transaction.amount) {
        transactionsIncome += parseFloat(transaction.amount);
      }
    });
    // if there is any income
    // let's store it and show to our user
    if (transactionsIncome) {
      dispatch({ type: USER_SYNC_DATA, payload: { transactionsIncome } });
    }
  } catch (e) {
    // actually we do not want to show any error to user
    // so it is only for debug
    console.warn(e);
  }
};
