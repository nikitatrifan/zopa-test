export const FETCH_TRANSACTIONS_REQUEST = '@transactions/fetch/request';
export const FETCH_TRANSACTIONS_SUCCESS = '@transactions/fetch/success';
export const FETCH_TRANSACTIONS_FAILURE = '@transactions/fetch/failure';

export const SEND_TRANSACTION_REQUEST = '@transactions/send/request';
export const SEND_TRANSACTION_SUCCESS = '@transactions/send/success';
export const SEND_TRANSACTION_FAILURE = '@transactions/send/failure';
