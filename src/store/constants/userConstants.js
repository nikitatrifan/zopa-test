export const USER_SYNC_DATA = '@user/sync-data';
export const USER_FETCH_BALANCE = '@user/fetch/balance';

export const USER_LOGIN_REQUEST = '@user/login/request';
export const USER_LOGIN_SUCCESS = '@user/login/success';
export const USER_LOGIN_FAILURE = '@user/login/failure';

export const USER_SIGNUP_REQUEST = '@user/signup/request';
export const USER_SIGNUP_SUCCESS = '@user/signup/success';
export const USER_SIGNUP_FAILURE = '@user/signup/failure';

export const USER_LOGOUT_REQUEST = '@user/logout/request';
export const USER_LOGOUT_SUCCESS = '@user/logout/success';
export const USER_LOGOUT_FAILURE = '@user/logout/failure';
