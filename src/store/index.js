import { createStore, compose, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import rootReducer from './reducers';
import noop from '../helpers/noop';

const isWeb = typeof window !== 'undefined';
const isDebug = process.env.NODE_ENV !== 'production';

export const configureStore = (preloadedState: Object): Object => {
  // eslint-disable-next-line
  const initialState = isWeb ? window.__REDUX_STATE__ || {} : preloadedState;

  const middleware = ((): Function => {
    const thunk = reduxThunk.default ? reduxThunk.default : reduxThunk;
    // eslint-disable-next-line
    if (process.env.NODE_ENV === 'production' || !isWeb || (isWeb && !isDebug)) {
      return applyMiddleware(thunk);
    }
    // eslint-disable-next-line
    const { createLogger } = require('redux-logger');
    const logger = createLogger({
      collapsed: true,
    });

    return applyMiddleware(thunk, logger);
  })();

  const devToolsExt = isWeb && window.devToolsExtension ? window.devToolsExtension() : noop;
  const middlewares = compose(
    middleware,
    devToolsExt,
  );
  const store = createStore(rootReducer, initialState, middlewares);

  if (module && module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./reducers', () => {
      // eslint-disable-next-line
      const nextRootReducer = require('./reducers').default;
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
};

export const store = configureStore();

if (isDebug && isWeb) {
  window.store = store;
}
