import {
  FETCH_TRANSACTIONS_REQUEST,
  FETCH_TRANSACTIONS_SUCCESS,
  FETCH_TRANSACTIONS_FAILURE,
  SEND_TRANSACTION_REQUEST,
  SEND_TRANSACTION_SUCCESS,
  SEND_TRANSACTION_FAILURE,
} from '../constants/transactionsConstants';
import type { ActionType, TransactionFieldsType } from '../../types';

export type TransactionsState = {
  isFetching: boolean,
  isError: boolean,
  errorMessage: string,
  data: Array<TransactionFieldsType>,
};

const initialState = {
  isFetching: false,
  isError: false,
  errorMessage: null,
  data: [],
};

export default function transactions(
  state: TransactionsState = initialState, action: ActionType,
): TransactionsState {
  const { type, payload = {} } = action;

  switch (type) {
    case FETCH_TRANSACTIONS_REQUEST:
    case SEND_TRANSACTION_REQUEST:
      return {
        ...state,
        isFetching: true,
        isError: false,
        errorMessage: null,
      };
    case SEND_TRANSACTION_FAILURE:
    case FETCH_TRANSACTIONS_FAILURE:
      return {
        ...state,
        isFetching: false,
        isError: true,
        errorMessage: payload,
      };
    case SEND_TRANSACTION_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        errorMessage: null,
        data: [...state.data, payload],
      };
    case FETCH_TRANSACTIONS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        errorMessage: null,
        data: payload,
      };
    default:
      return state;
  }
}
