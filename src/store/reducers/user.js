import {
  USER_SYNC_DATA,
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE,
  USER_SIGNUP_REQUEST,
  USER_SIGNUP_SUCCESS,
  USER_SIGNUP_FAILURE,
  USER_LOGOUT_REQUEST,
  USER_LOGOUT_SUCCESS,
  USER_LOGOUT_FAILURE,
} from '../constants/userConstants';
import type { ActionType, UserFieldsType } from '../../types';

export type UserState = UserFieldsType & {
  isFetching: boolean,
  isAuthenticated: boolean,
  isError: boolean,
  errorMessage: string,
};

const initialState = {
  isFetching: false,
  isError: false,
  isAuthenticated: false,
  errorMessage: null,
  email: null,
  password: null,
};

export default function user(state: UserState = initialState, action: ActionType): UserState {
  const { type, payload = {} } = action;

  switch (type) {
    case USER_SYNC_DATA:
      return {
        ...state,
        ...payload,
      };
    case USER_LOGIN_REQUEST:
    case USER_SIGNUP_REQUEST:
      return {
        ...state,
        ...payload,
        isFetching: true,
        isError: false,
        errorMessage: null,
        isAuthenticated: false,
      };
    case USER_LOGIN_FAILURE:
    case USER_SIGNUP_FAILURE:
      return {
        ...state,
        isFetching: false,
        isError: true,
        errorMessage: payload,
      };
    case USER_LOGIN_SUCCESS:
    case USER_SIGNUP_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        isAuthenticated: true,
        errorMessage: null,
      };
    case USER_LOGOUT_REQUEST:
      return {
        ...state,
        isFetching: false,
        isError: false,
        isAuthenticated: false,
        errorMessage: null,
      };
    case USER_LOGOUT_SUCCESS:
    case USER_LOGOUT_FAILURE:
    default:
      return state;
  }
}
