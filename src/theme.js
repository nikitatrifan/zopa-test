export default {
  mainFont: 'Roboto, SF UI Display, Helvetica, Roboto, sans-serif',
  secondaryFont: 'Alverata, sans-serif',
  monoFont: 'Fira Code, monospace',

  textColor: '#1E2441',
  whiteColor: '#ffffff',
  lightGrayColor: '#FAFAFA',
  darkBackground: '#27292A',
  introBackground: '#17181a',
  redColor: '#D6061E',

  smallMargin: '12px 0 8px 0',
  mediumMargin: '32px 0 14px 0',
  largeMargin: '72px 0 32px 0',

  primaryColor: '#5F5AF1',
  primaryLightColor: '#9893F9',

  mobilePoint: 600,
  tabletPoint: 1060,
  desktopPoint: 1200,

  logoName: 'Zopa',
};

export type MarginType = 'small' | 'medium' | 'large';
export type StylesType = { [key: string]: Object } | number | false | null; // eslint-disable-line
