export type ActionType = {
  payload: any,
  type: string,
};

export type UserFieldsType = {
  name: string,
  email: string,
  password: string,
  balance: number,
  transactionsIncome: number,
};

export type TransactionFieldsType = {
  amount: number,
  createdAt: Date,
  recipientEmail: string,
  recipientName: string,
  senderEmail: string,
};
